﻿using System;
using System.Diagnostics;
using System.Linq;

namespace ParallelLINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            int threadsCount = 8;
            int[] itemsCount = { 100_000, 1_000_000, 10_000_000 };

            foreach(int items in itemsCount)
            {
                // Инициализация массива чисел int
                long[] numbers = new long[items];
                InitNumbers(numbers);
                // Обычное сложение
                Usual.SerialAdd(numbers, items);
                // Параллельное сложение с использованием List<Thread>
                Threads threads = new Threads(numbers, threadsCount, items);
                threads.ThreadList();
                // Сложение с использованием Linq в один поток
                LinqAdd.SerialLinq(numbers);
                // Сложение с использованием Parallel Linq в _threadsCount потоков
                LinqAdd.ParallelLinq(numbers);
                Console.WriteLine(); 
            }
            Console.ReadKey();
        }


        private static void InitNumbers(long[] numbers)
        {
            var rand = new Random();
            int num = numbers.Count();
            for (int i = 0; i < num; i++)
            {
                numbers[i] = rand.Next(1, 100);
            }
            Console.WriteLine($"Инициализирован массив int c количеством элементов: {num}");
        }

    }
}
