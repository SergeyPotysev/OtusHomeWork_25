﻿using System;
using System.Diagnostics;

namespace ParallelLINQ
{
    public class Usual
    {
        public static void SerialAdd(long[] numbers, int items)
        {
            long sum = 0;
            var sw = Stopwatch.StartNew();
            for (int i = 0; i < items; i++)
            {
                sum += numbers[i];
            }
            sw.Stop();
            double ms = (double)sw.ElapsedTicks / (double)TimeSpan.TicksPerMillisecond;
            Console.WriteLine($"Время, потраченное на последовательное сложение элементов массива int: {ms} ms");
            Console.WriteLine($"Полученная сумма: {sum}");
        }

    }
}
