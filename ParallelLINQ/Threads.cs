﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace ParallelLINQ
{
    class Threads
    {
        static int _items;
        static long[] _numbers;
        static int _threadsCount;
        static long[] _partSum;
        static int _range;
        
        public Threads(long[] numbers, int threadsCount, int items)
        {
            _numbers = numbers;
            _threadsCount = threadsCount;
            _items = items;
            _partSum = new long[_threadsCount];
            _range = items / _threadsCount;
        }


        public void ThreadList()
        {
            long sum = 0;
            var sw = Stopwatch.StartNew();
            List<Thread> threads = new List<Thread>();
            for (int part = 0; part < _threadsCount; part++)
            {
                Thread thread = new Thread(new ParameterizedThreadStart(RangeSum));
                thread.Name = "SID" + part;
                thread.Start(part);
                threads.Add(thread);
            }
            foreach (Thread thread in threads)
            {
                thread.Join();
            }
            sum = _partSum.Sum();
            sw.Stop();
            double ms = (double)sw.ElapsedTicks / (double)TimeSpan.TicksPerMillisecond;
            Console.WriteLine($"Время, потраченное на сложение элементов массива int с помощью List<Thread>: {ms} ms");
            Console.WriteLine($"Полученная сумма: {sum}");
        }


        private static void RangeSum(object x)
        {
            int part = (int)x;
            int from = _range * part;
            int to = ((part + 1) == _threadsCount) ? _items : _range * (part + 1);
            for (int i = from; i < to; i++)
            {
                _partSum[part] += _numbers[i];
            }
        }

    }
}
