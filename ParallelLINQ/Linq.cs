﻿using System;
using System.Diagnostics;
using System.Linq;

namespace ParallelLINQ
{
    public class LinqAdd
    {
        public static void SerialLinq(long[] numbers)
        {
            var sw = Stopwatch.StartNew();
            long sum = numbers.Sum();
            sw.Stop();
            double ms = (double)sw.ElapsedTicks / (double)TimeSpan.TicksPerMillisecond;
            Console.WriteLine($"Время, потраченное на сложение элементов массива int с помощью Linq: {ms} ms");
            Console.WriteLine($"Полученная сумма: {sum}");
        }


        public static void ParallelLinq(long[] numbers)
        {
            var sw = Stopwatch.StartNew();
            long sum = numbers.AsParallel().Sum();
            sw.Stop();
            double ms = (double)sw.ElapsedTicks / (double)TimeSpan.TicksPerMillisecond;
            Console.WriteLine($"Время, потраченное на сложение элементов массива int с помощью Parallel Linq: {ms} ms");
            Console.WriteLine($"Полученная сумма: {sum}");
        }

    }
}
